const port = process.env.PORT || 80
const pollingRate = process.env.POLLING_RATE || 500
const webSocketServer = require('websocket').server
const http = require('http')
const { inspect } = require('util')

const firebase = require('firebase/app')
const db = require('./Firebase.js')
const { serverTimestamp, collection, doc, onSnapshot, orderBy, limit, addDoc, updateDoc, query, writeBatch } = require('firebase/firestore')

const state = {
    chatrooms: {}
}

let wsServer = undefined

const collectionRef = collection(db, 'chatroom')

notifyClients = (data) => {
    wsServer.broadcastUTF(JSON.stringify(data))
}

populateChatroom = async () => {
    return new Promise((resolve) => {
        // creating chatroom listener
        const q = query(collectionRef, limit(10000))
        onSnapshot(q, (snapshot) => {
            snapshot.docChanges().forEach(function (change) {
                console.debug(`'change' parameter: ${inspect(change)}`)
                if (change.type === 'added') {
                    notifyClients({ type: "new-room", id: change.doc.id, data: change.doc.data({ serverTimestamps: 'estimate' }) })
                    state.chatrooms[change.doc.id] = ({ data: change.doc.data({ serverTimestamps: 'estimate' }), chats: [] })
                    populateChats(change.doc.id)
                } else if (change.type === 'modified') {
                    notifyClients({ type: "rename-room", id: change.doc.id, name: change.doc.data()['name'] })
                    state.chatrooms[change.doc.id].data['name'] = change.doc.data()['name']
                }
            })
            resolve()
        })
    })
}

populateChats = async (id) => {
    return new Promise((resolve) => {
        // creating chat listener
        const q = query(collection(doc(collectionRef, id), "chats"), orderBy('timestamp'))
        onSnapshot(q, (snapshot) => {
            if (state.chatrooms[id].chats.length === 0) {
                state.chatrooms[id].chats = {}
                snapshot.forEach(function (doc) {
                    state.chatrooms[id].chats[doc.id] = { id: doc.id, data: doc.data({ serverTimestamps: 'estimate' }) }
                })
            } else {
                snapshot.docChanges().forEach(function (change) {
                    if ((change.type === 'modified' && change.doc.data()['from_admin']) || (change.type === 'added' && !change.doc.data()['from_admin'] && !change.doc.data()['read'])) {
                        notifyClients({ type: "recieve-chat", chatroom: id, id: change.doc.id, data: change.doc.data({ serverTimestamps: 'estimate' }) })
                        state.chatrooms[id].chats[change.doc.id] = { id: change.doc.id, data: change.doc.data({ serverTimestamps: 'estimate' }) }
                    }
                })
            }
            resolve()
        })
    })
}

handleChatSubmit = async (chat, activeRoom) => {
    return new Promise((resolve) => {
        addDoc(collection(doc(collectionRef, activeRoom), 'chats'), {
            content: chat,
            from_admin: true,
            read: true,
            timestamp: serverTimestamp()
        })
        resolve()
    })
}

clearBadge = async (name) => {
    return new Promise((resolve) => {
        let batch = writeBatch(db)
        for (let id in state.chatrooms[name].chats) {
            batch.update(
                doc(collection(doc(collectionRef, name), 'chats'), id),
                { read: true })
        }
        batch.commit()
        resolve()
    })
}

renameRoom = async (oldName, newName) => {
    return new Promise((resolve) => {
        updateDoc(doc(collectionRef, oldName), {
            name: newName.trim()
        })
        resolve()
    })
}

// populate
populateChatroom().then(() => {
    console.log((new Date()) + " Done populating chatroom!")
})

// ---------- init http server ----------
var server = http.createServer((request, response) => { })
server.listen(port, () => {
    console.log((new Date()) + " Server is listening on port " + port)
})

// init websocket server
wsServer = new webSocketServer({ httpServer: server })
wsServer.on('request', function (request) {
    var connection = request.accept(null, request.origin)

    console.info((new Date()) + ' Connection from ' + connection.socket.remoteAddress + ' accepted. Active clients: ' + wsServer.connections.length)
    console.debug(`'state' object: ${inspect(state)}`);

    if (state.chatrooms !== {}) {
        connection.sendUTF(JSON.stringify({ type: 'history', data: state.chatrooms }))
    }

    // client sent message
    connection.on('message', function (message) {
        console.debug(`'message' parameter: ${inspect(message)}`)
        
        if (message.type === 'utf8') {
            var json
            try {
                json = JSON.parse(message.utf8Data)
            } catch (e) {
                console.log('Invalid JSON: ', message.utf8Data)
                return
            }

            // parse message
            if (json.type === 'clear-badge') {
                console.log((new Date()) + " Clearing badge: " + json.chatroom)
                clearBadge(json.chatroom).then(notifyClients({ type: "clear-badge", chatroom: json.chatroom }))
            } else if (json.type === 'send-chat') {
                console.log((new Date()) + " Send chat: " + json.data + " @ " + json.chatroom)
                handleChatSubmit(json.data, json.chatroom)
            } else if (json.type === 'rename-room') {
                console.log((new Date()) + " Rename room: " + json.old + "->" + json.new)
                renameRoom(json.old, json.new)
            } else if (json.type === 'ping') {
                connection.sendUTF(JSON.stringify({ type: "pong" }))
            } else {
                console.log((new Date()) + " Unkown command: " + json.type)
            }
        }
    })

    // client disconnected
    connection.on('close', function (connection) {
        console.log((new Date()) + " Peer " + connection.toString() + " disconnected. Active clients: " + wsServer.connections.length)
    })
})
