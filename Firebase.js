const { initializeApp } = require('firebase/app')
const { getFirestore } = require('firebase/firestore')

const config = {
    apiKey: 'AIzaSyBOX1uI3p6nN0XOTxR-P0xek-jajQj1Tng',
    projectId: 'qir-ftui'
};

const app = initializeApp(config)
const db = getFirestore(app)

module.exports = db
